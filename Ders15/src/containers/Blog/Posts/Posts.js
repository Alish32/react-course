import React, { Component } from 'react'
import axios from '../../../axios'
import Post from '../../../components/Post/Post'
import { Link } from 'react-router-dom'
import './Posts.css'

class Posts extends Component {
    state = {
        posts: [],
        error: false
    }

    componentDidMount() {
        console.log(this.props)
        axios.get('/posts').then((response) => {
            const posts = response.data.slice(0, 4)
            const updatedPosts = posts.map((post) => {
                return {
                    ...post,
                    author: 'Alish'
                }
            })

            this.setState({
                posts: updatedPosts
            })

        })
            .catch((error) => {
                this.setState({
                    error: true
                })
            })
    }

    postSelectedHandler(id) {
        this.props.history.push({pathname: '/posts/' + id})
    }

    render() {
        let posts = <p style={{ textAlign: "center" }}>Something went wrong</p>
        if (!this.state.error) {
            posts = this.state.posts.map((post) => {
                return (
                        <Post key={post.id} title={post.title} author={post.author} clicked={() => {
                        this.postSelectedHandler(post.id)
                    }} />
                )
            })
        }

        return (
            <section className="Posts">
                {posts}
            </section>
        )
    }
}

export default Posts