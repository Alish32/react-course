import React, { Component } from 'react';

import './Blog.css';
import Posts from './Posts/Posts'
import { Route, NavLink, Switch } from 'react-router-dom';
import NewPost from './NewPost/NewPost';
import FullPost from './FullPost/FullPost';

class Blog extends Component {

    render() {
        return (
            <div className="Blog">
                {/* header ul li */}
                <header>
                    <nav>
                        <ul>
                            <li><NavLink to="/"
                                exact
                                activeClassName="my-active"
                                activeStyle={
                                    {
                                        color: 'red'
                                    }
                                }
                            >Home</NavLink></li>
                            <li><NavLink to={
                                {
                                    pathname: '/new-post',
                                    hash: '#submit',
                                    search: '?asdf=true'
                                }
                            }>New Post</NavLink></li>
                        </ul>
                    </nav>
                </header>
                {/* <Posts /> */}
                <Switch>
                    <Route path='/' exact component={Posts} />
                    <Route path='/new-post' component={NewPost} />
                    <Route path='/posts/:id' component={FullPost} />
                </Switch>
            </div>
        );
    }
}   

export default Blog;