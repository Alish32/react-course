import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from './store/reducer'


axios.defaults.baseURL = 'http://jsonplaceholder.typicode.com'
axios.defaults.headers.common['Authorization'] = 'AUTH TOKEN'

axios.interceptors.request.use(request => {
    console.log(request)
    return request
}, error => {
    console.log(error)
    return Promise.reject(error) // bunnu edirik ki istesek 
                                    //oz componentimizde de erroru tuta bilek
})

axios.interceptors.response.use(response => {
    console.log(response)
    return response
}, error => {
    console.log(error)
    return Promise.reject(error) // bunnu edirik ki istesek 
                                    //oz componentimizde de erroru tuta bilek
})

const store = createStore(reducer)

ReactDOM.render( <Provider store={store}><App /></Provider>, document.getElementById( 'root' ) );
registerServiceWorker();
