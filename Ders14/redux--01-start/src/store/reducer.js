import * as actionTypes from './action'

const initialState = {
    counter: 0,
    results: []
}

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.INCREMENT:
            return {
                ...state,
                counter: state.counter + 1
            }
        case actionTypes.DECREMENT:
            return {
                ...state,
                counter: state.counter - 1
            }
        case actionTypes.ADD:
            return {
                ...state,
                counter: state.counter + 5
            }
        case actionTypes.SUBTRACT:
            return {
                ...state,
                counter: state.counter - 9
            }
        case actionTypes.STORE_RESULT:
            return {
                ...state,
                results: state.results.concat({ id: new Date(), val: state.counter })
            }
        case actionTypes.DELETE_RESULT:
            // const index = 2
            //state.results.splice(index, 1)
            const newArray = state.results.filter(result => result.id !== action.resultElId)
            return {
                ...state,
                results: newArray
            }
    }
    return state
}

export default reducer