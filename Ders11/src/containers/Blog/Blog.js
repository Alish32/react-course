import React, { Component } from 'react';

import Post from '../../components/Post/Post';
import FullPost from '../../components/FullPost/FullPost';
import NewPost from '../../components/NewPost/NewPost';
import './Blog.css';
import axios from 'axios'

class Blog extends Component {
    state = {
        gelenPosts: [],
        selectedPostId: null
    }

    componentDidMount() {
        axios.get('http://jsonplaceholder.typicode.com/posts').then(response => {

            const posts = response.data.slice(0, 4)
            const updatedPosts = posts.map(post => {
                return {
                    ...post,
                    author: 'ASDF'
                }
            })

            this.setState({
                gelenPosts: updatedPosts
            })

        })
    }

    postSelectedHandler(id) {
        this.setState({
            selectedPostId: id
        })
    }


    render() {
        const posts = this.state.gelenPosts.map(post => {
            return <Post key={post.id} title={post.title} author={post.author} clicked={() =>
                this.postSelectedHandler(post.id)
            } />
        })

        return (
            <div>
                <section className="Posts">
                    {posts}
                </section>
                <section>
                    <FullPost id={this.state.selectedPostId} />
                </section>
                <section>
                    <NewPost />
                </section>
            </div>
        );
    }
}

export default Blog;