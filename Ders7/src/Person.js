import React from 'react'
import classes from './Person.css'

const person = (props) => {
    if (Math.random() > 0.9) {
        throw new Error("Someting went wrong...")
    }
    return (
        <div className={classes.Person}>
            <h1>{props.children}</h1>
            <p>I'm {props.name} and I am {props.age}</p>
            <input type="text" onChange={props.changed} value={props.name} />
            <button onClick={props.deletePerson}>Delete</button>
        </div>
    )
}

export default person;