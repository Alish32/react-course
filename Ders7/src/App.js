import React, { Component } from 'react';
import classes from './App.css';
import Person from './Person'
import ErrorBoundry from './ErrorBoundry/ErrorBoundry'

class App extends Component {
  state = {
    persons: [
      { id: 1, name: 'Alish', age: 12 },
      { id: 2, name: 'Alish1', age: 29 },
      { id: 3, name: 'Alish2', age: 39 },
    ],
    showPerson: true
  }

  showPersonHandler = () => {
    console.log("show person clicked..." + this.state.showPerson)
    this.setState({
      showPerson: !this.state.showPerson
    })
  }

  changeNameHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.pid === id
    })
    console.log(personIndex)

    const person = this.state.persons[personIndex]
    person.name = event.target.value
    const persons = this.state.persons

    persons[personIndex] = person

    this.setState({
      persons: persons
    })
  }

  deletePerson = (index) => {
    console.log("deleted..." + index)
    const persons = [...this.state.persons]
    persons.splice(index, 1)
    this.setState({
      persons: persons
    })
  }

  render() {
    let person = null
    let btnClass = [classes.Button]
    console.log(btnClass)
    if (this.state.showPerson) {
      person = (
        <div>
          {
            this.state.persons.map((person, index) => {
              return <ErrorBoundry key={person.id} ><Person 
              name={person.name} 
              age={person.age} 
              key={person.id} 
              changed={(event) => this.changeNameHandler(event, person.id)}
              deletePerson={() => this.deletePerson(index)} >I am a child props...</Person></ErrorBoundry>
            }
            )
          }
        </div>
      )
    btnClass.push(classes.Red)
    console.log(btnClass)
    }

    

    return (
      <div className={classes.App}>
        {
          (this.state.persons.length) ?
          <button className={btnClass.join(' ')} onClick={this.showPersonHandler}>Show/False</button>
          : <div>bos</div>
        }
        {person}
      </div>
    )
  }
}

export default App;