import React, { Component } from 'react';
import classes from './App.css';
import Persons from '../components/Persons/Persons'
import Cockpit from '../components/cockpit/cockpit'

class App extends Component {
  constructor(props){
    super()
    console.log('App.js --- constructor')
  }
  state = {
    persons: [
      { id: 1, name: 'Alish', age: 12 },
      { id: 2, name: 'Alish1', age: 29 },
      { id: 3, name: 'Alish2', age: 39 },
    ],
    showPerson: true
  }

  static getDerivedStateFromProps(props, state){
    console.log('App.js --- getDerivedStateFromProps', props)
    return state
  }

  componentDidMount(){
    console.log('App.js --- componentDidMount')
  }

  showPersonHandler = () => {
    console.log("show person clicked..." + this.state.showPerson)
    this.setState({
      showPerson: !this.state.showPerson
    })
  }

  changeNameHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id
    })
    console.log(personIndex)

    const person = this.state.persons[personIndex]
    person.name = event.target.value
    const persons = this.state.persons

    persons[personIndex] = person

    this.setState({
      persons: persons
    })
  }

  deletePerson = (index) => {
    console.log("deleted..." + index)
    const persons = [...this.state.persons]
    persons.splice(index, 1)
    this.setState({
      persons: persons
    })
  }

  render() {
    console.log('App.js --- render')
    let person = null
    if (this.state.showPerson) {
      person = (
        <div>
          <Persons persons={this.state.persons} changed={this.changeNameHandler} clicked={this.deletePerson}/>
        </div>
      )
    }



    return (
      <div className={classes.App}>
        <Cockpit title={this.props.appTitle} showPerson={this.state.showPerson} clicked={this.showPersonHandler} />
        {person}
      </div>
    )
  }
}

export default App;