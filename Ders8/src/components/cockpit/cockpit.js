import React from 'react'
import classes from './cockpit.css'

const cockpit = (props) => {

    let btnClass = ''

    if (props.showPerson) {
        btnClass = classes.Red
    }
    return (
        <div className={classes.Cockpit}>
            <h1>Hi I'm a React App</h1>
            <p>{props.title}</p>
            <button className={btnClass} onClick={props.clicked}>Show/False</button>
        </div>
    )
}

export default cockpit