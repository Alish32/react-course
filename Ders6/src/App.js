import React, { Component } from 'react';
import Navbar from './components/Navbar/Navbar';
import TaskAdd from './components/Tasks/TaskAdd/TaskAdd';
import Tasks from './components/Tasks/Tasks';
import './App.css';
class App extends Component {

  state = {
    tasksRemain: ['Project - X bugs', 'Shopping'],
    addTask: '',
    darkMode: false
  };

  onChangeHandler = (event) => {
    console.log("changed...")
    this.setState({
      addTask: event.target.value
    });
  }

  addTaskHandler = () => {
    console.log("clicked...")
    this.setState({
      tasksRemain: [...this.state.tasksRemain, this.state.addTask],
      addTask: ''
    });
  }

  removeTaskHandler = index => {
    const beforeTasks = this.state.tasksRemain.slice(0, index);
    const afterTasks = this.state.tasksRemain.slice(index + 1);
    const tasks = [...beforeTasks, ...afterTasks];
    console.log(tasks);
    this.setState({ tasksRemain: tasks });
  }

  darkModeHandler = () => {
    this.setState({
      darkMode: !this.state.darkMode
    })
    console.log("Dark mode: " + this.state.darkMode)
  }

  render() {
    // Adding classes dynamically     
    let buttonClasses = []
    // Pushing bold class into the array     

    buttonClasses.push('bold');
    // Checking the Mode and dynamically pushing a CSS class     

    if (this.state.darkMode) {
      buttonClasses.push('green-bg', 'black-font');
      console.log(buttonClasses);    
    } else {
      buttonClasses.push('orange-bg', 'black-font');
      console.log(buttonClasses);    
    }

    // Button style for Dark Mode Option     

    let buttonStyle = {
      padding: '10px',
      border: '1px solid #000000',
      borderRadius: '10px',
      outline: 'none',
      display: 'block',
      margin: '10px auto',
      fontSize: '100%'
    }

    // Inline Styling for Background color     
    let bColor = {
      backgroundColor: '#ffffff',
      color: '#000000',
      height: '100vh'
    }
    console.log(bColor)

    if(this.state.darkMode) {      
      bColor = {        
          backgroundColor: '#2d3436',        
          color: '#ffffff',        
          height: '100vh'      
      }; 
      
   }
   console.log(bColor)

    return (
      <div style={bColor}>
        <Navbar />
        <button style={buttonStyle} onClick={this.darkModeHandler}
          className={buttonClasses.join(' ')}>
          {this.state.darkMode ? 'Light' : 'Dark'} Mode </button>
        <TaskAdd
          changed={this.onChangeHandler}
          clicked={this.addTaskHandler}
        />
        {this.state.tasksRemain.length === 0 ?

          <p
            className={this.state.darkMode ? 'no-task-alert dark-mode' : 'no-task-alert light-mode'}>
            No Tasks
           </p> :
          <Tasks
            tasks={this.state.tasksRemain}
            remove={this.removeTaskHandler}
            darkMode={this.state.darkMode} />
        }
      </div>
    )
  }
}
export default App;