import React from 'react';
import './Task.css';
const Task = (props) => {
    let styleText = {
        fontSize: '160%'
    };
    let styleCompletedButton = {
        backgroundColor: '#00b894',
        fontWeight: 'bold',
        padding: '10px',
        margin: '15px 0',
        border: 'transparent',
        borderRadius: '5px',
        outline: 'none',
        fontSize: '100%'
    };
    return (
        <li>
            <p style={styleText}>

                {props.task}
            </p>

            <button
                style={styleCompletedButton}
                onClick={props.remove}>
                Completed
            </button>
        </li>
    )
}
export default Task;