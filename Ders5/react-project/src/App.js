import React, { Component } from 'react';
import './App.css';
import Person from './Person'
import styled from 'styled-components';

const StyledButton = styled.button`
  background-color: ${props => props.alt ? 'red' : 'green'};
  color: white;
  font: inherit;
  border: 1px solid blue;
  padding: 8px;
  cursor: pointer;

  &:hover {
    background-color: ${props => props.alt ? 'salmon' : 'lightgreen'};
  }
`
class App extends Component {
  state = {
    persons: [
      { id: 1, name: 'Alish', age: 12 },
      { id: 2, name: 'Alish1', age: 29 },
      { id: 3, name: 'Alish2', age: 39 },
    ],
    showPerson: true
  }

  showPersonHandler = () => {
    console.log("show person clicked..." + this.state.showPerson)
    this.setState({
      showPerson: !this.state.showPerson
    })
  }

  changeNameHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id
    })
    console.log(personIndex)

    const person = this.state.persons[personIndex]
    person.name = event.target.value
    const persons = this.state.persons

    persons[personIndex] = person

    this.setState({
      persons: persons
    })
  }

  deletePerson = (index) => {
    console.log("deleted..." + index)
    const persons = [...this.state.persons]
    persons.splice(index, 1)
    this.setState({
      persons: persons
    })
  }

  render() {
    let person = null

    if (this.state.showPerson) {
      person = (
        <div>
          {
            this.state.persons.map((person, index) => {
              return <Person 
              name={person.name} 
              age={person.age} 
              key={person.id} 
              changed={(event) => this.changeNameHandler(event, person.id)}
              deletePerson={() => this.deletePerson(index)}/>
            }
            )
          }
        </div>
      )

    }

    

    return (
      <div className="App">
        {
          (this.state.persons.length) ?
          <StyledButton  alt={this.state.showPerson} onClick={this.showPersonHandler}>Show/False</StyledButton>
          : <div>bos</div>
        }
        {person}
      </div>
    )
  }
}

export default App;