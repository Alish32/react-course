import React, { Component } from 'react';
import './App.css';
import Person from './Person'
import person from './Person';

class App extends Component {
  state = {
    persons: [
      { name: 'Alish', age: 12 },
      { name: 'Alish1', age: 29 },
      { name: 'Alish2', age: 39 },
    ]
  }

  render() {
    return (
      <div className="App">
        {
          this.state.persons.map((person) => {
            return <Person name={person.name} age={person.age} />
          }
          )
        }
      </div>
    )
  }
}

export default App;