import React, { Component } from 'react';
import './App.css';
import Person from './Person'

class App extends Component {
  state = {
    persons: [
      { id: 1, name: 'Alish', age: 12 },
      { id: 2, name: 'Alish1', age: 29 },
      { id: 3, name: 'Alish2', age: 39 },
    ],
    showPerson: true,
    asd: [1, 1, 1]
  }

  showPersonHandler = () => {
    console.log("show person clicked..." + this.state.showPerson)
    this.setState({
      showPerson: !this.state.showPerson
    })
  }

  changeNameHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id
    })
    console.log(personIndex)

    const person = this.state.persons[personIndex]
    person.name = event.target.value
    const persons = this.state.persons

    persons[personIndex] = person

    this.setState({
      persons: persons
    })
  }

  deletePerson = (index) => {
    console.log("deleted..." + index)
    const persons = [...this.state.persons]
    persons.splice(index, 1)
    this.setState({
      persons: persons
    })

  }

  showA = (index) => {
    let as = this.state.asd
    as[index] = !as[index]
    console.log(as)
    this.setState({
      asd: as
    })

  }

  render() {
    let person = null
    let button = null
    if (this.state.showPerson) {
      person = (
        <div>
          {
            this.state.persons.map((person, index) => {
              return (this.state.asd[index]) ? <Person
                name={person.name}
                age={person.age}
                key={person.id}
                changed={(event) => this.changeNameHandler(event, person.id)}
            deletePerson={() => this.deletePerson(index)} /> : <div key={person.id}>{index + 1} hided</div>
            }
            )
          }
        </div>
      )
    }

    button = (
      <div>
        {
          this.state.persons.map((person, index) => {
            return <button onClick={() => this.showA(index)} key={index}
            >show/hide {index + 1}</button>
          }
          )
        }
      </div>
    )
    return (
      <div className="App">
        {
          (this.state.persons.length) ?
            <button onClick={this.showPersonHandler}>Show/Hide</button>
            : <div>bos</div>
        }
        {button}

        {person}
      </div>
    )
  }
}

export default App;