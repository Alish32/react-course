import React, { Component } from 'react'
import classes from './Person.css'

class Person extends Component {

    render() {
        console.log('Person.js --- rendering...')
        return (
            <div className={classes.Person}>
                <h1>{this.props.children}</h1>
                <p>I'm {this.props.name} and I am {this.props.age}</p>
                <input type="text" onChange={this.props.changed} value={this.props.name} />
                <button onClick={this.props.deletePerson}>Delete</button>
            </div>
        )
    }

}

export default Person;