import React, {useEffect} from 'react'
import classes from './cockpit.css'

const cockpit = (props) => {
    useEffect(() => {
        console.log('cockpit.js --- useEffect')

        //hhtp getpersons

        // setTimeout(() => {
        //     alert('data saved to cloud')
        // }, 1000)

        return(() => {
            console.log('cockpit.js --- unmount')
        })

    }, [props.persons])

    let btnClass = ''

    if (props.showPerson) {
        btnClass = classes.Red
    }
    return (
        <div className={classes.Cockpit}>
            <h1>Hi I'm a React App</h1>
            <p>{props.title}</p>
            <button className={btnClass} onClick={props.clicked}>Show/False</button>
        </div>
    )
}

export default cockpit