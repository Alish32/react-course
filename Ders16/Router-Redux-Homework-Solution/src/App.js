import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom'

import { Route, NavLink, Switch } from 'react-router-dom';
import Hourly from './containers/Hourly/Hourly';
import FiveDays from './containers/FiveDays/FiveDays';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <header>
            <nav>
              <ul>
                <li><NavLink to="/" exact activeClassName="my-active"  activeStyle={ { color: 'red' } } >Home</NavLink></li>
                <li><NavLink to={
                  {
                    pathname: '/hourly',
                    hash: '#submit',
                    search: '?asdf=true'
                  }
                }
                  exact
                  activeClassName="my-active"
                  activeStyle={
                    {
                      color: 'red'
                    }}>Hourly</NavLink></li>
              </ul>
            </nav>
          </header>
          <Route path='/' exact component={FiveDays} />
          <Switch>
            <Route path='/' component={Hourly} />
            <Route path='/hourly/:id' component={Hourly} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
