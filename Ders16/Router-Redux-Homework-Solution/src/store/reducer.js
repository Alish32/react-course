import * as actionTypes from './action'

const initialState = {
    weather: null,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_WEATHER:
            return {
                weather: action.value
            }
        default: 
            return state
    }
}

export default reducer