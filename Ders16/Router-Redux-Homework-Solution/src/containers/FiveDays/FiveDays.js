import React, { Component } from 'react';
import { connect } from 'react-redux'
import './FiveDays.css';
import axios from 'axios'
import * as actionTypes from '../../store/action'


class FiveDays extends Component {
    state = {
        weatherArray: [],
        city: ''
    }

    componentDidUpdate(previousProps, previousState) {
        if (previousState.city !== this.state.city) {
            const url = 'http://api.openweathermap.org/data/2.5/forecast?units=metric&q=' + this.state.city + '&APPID=9a2375a362d85bc3162ff0e16dc913a5'
            axios.get(url).then(data => {
                let myDay = null
                let info = []
                let weather = {}
                let weather2 = []
                console.log(data);

                data.data.list.forEach(element => {

                    const [day, time] = element.dt_txt.split(' ')
                    if (day === myDay || !myDay) {
                        info.push(
                            {
                                time: time,
                                temp: Math.round(element.main.temp),
                                icon: element.weather[0].icon,
                            }
                        )
                    } else {
                        weather.myDay = day
                        weather.info = info
                        weather2.push({ ...weather })
                        info = []
                    }
                    myDay = day
                });

                this.setState({
                    weatherArray: weather2
                })

            }).catch(error => {
                
            })
        }
    }

    handleChange(event) {
        this.props.onSetDate('')
        this.setState({
            weatherArray: [],
            city: event.target.value
        })
    }

    render() {
        return (
            <div className='FiveDays'>
                <input type="text" onChange={(e) => { this.handleChange(e) }} value={this.state.city}></input>
                <div className='flex-container'>
                    {this.state.weatherArray.map(w => {
                        return <div key={w.myDay}>
                            <div onClick={() => this.props.onSetDate(w)}>{w.myDay}</div>
                        </div>
                    })}
                </div>
            </div >

        );
    }
}

const mapStateToProps = state => {
    return {
        weather: state.weather,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSetDate: (ww) => dispatch({
            type: actionTypes.SET_WEATHER,
            value: ww
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FiveDays);
