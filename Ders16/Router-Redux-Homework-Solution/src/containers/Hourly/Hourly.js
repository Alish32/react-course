import React, { Component } from 'react';
import { connect } from 'react-redux'
import './Hourly.css';

class Hourly extends Component {
    render() {
        return (
            <div >
                <p>2</p>
                {
                    (this.props.weather) ?
                        this.props.weather.info.map((p, index) => {
                            return <div className="card" key={index}>
                                <img src={"http://openweathermap.org/img/wn/" + p.icon + "@2x.png"} alt="img" />
                                <div className="container">
                                    <h4><b>{p.time}</b></h4>
                                    <p>{p.temp}</p>
                                </div>
                            </div>
                        }) : null
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        weather: state.weather
    }
}


export default connect(mapStateToProps)(Hourly);