import React, {useEffect, useContext} from 'react'
import classes from './cockpit.css'
import AuthContext from '../../context/auth-context'

const cockpit = (props) => {
    const context = useContext(AuthContext)
    useEffect(() => {
        console.log('cockpit.js --- useEffect')

        //hhtp getpersons

        // setTimeout(() => {
        //     alert('data saved to cloud')
        // }, 1000)

        return(() => {
            console.log('cockpit.js --- unmount')
        })

    }, [props.persons])

    let btnClass = ''

    if (props.showPerson) {
        btnClass = classes.Red
    }
    return (
        <div className={classes.Cockpit}>
            <h1>Hi I'm a React App</h1>
            <p>{props.title}</p>
            <button className={btnClass} onClick={props.clicked}>Show/False</button>
                {
                <button onClick={context.login}>Login/Logout</button>
                }
            
        </div>
    )
}

export default cockpit