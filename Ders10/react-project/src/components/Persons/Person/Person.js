import React, { Component } from 'react'
import classes from './Person.css'
import PropTypes from 'prop-types'
import AuthContext from '../../../context/auth-context'

class Person extends Component {
    static contextType = AuthContext

    render() {
        console.log('Person.js --- rendering...')
        return (
            <div className={classes.Person}>
                <h1>{this.props.children}</h1>
                <p>I'm {this.props.name} and I am {this.props.age}</p>
                    {
                        (this.context.authenticated) ? <p>Giris oldu!</p> : <p>Cixis oldu!</p>
                    }
                <input type="text" onChange={this.props.changed} value={this.props.name} />
                <button onClick={this.props.deletePerson}>Delete</button>
            </div>
        )
    }

}

Person.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number
}

export default Person;